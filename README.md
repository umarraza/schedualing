
# Schdeualing Project - Laravel 6

## Required packages

- Laravel Permission https://github.com/spatie/laravel-permission

## Setup Instruction
- composer update
- php artisan key:generate
- Create Database
- php artisan migrate
- php artisan db:seed
- php artisan storage:link