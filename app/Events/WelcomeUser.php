<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated.
 */
class WelcomeUser
{
    use SerializesModels;

    /**
     * @var
     */
    public $user;

    /**
     * @param $user
     */
    public function __construct($user)
    {   
        $this->user = $user;
    }
}
