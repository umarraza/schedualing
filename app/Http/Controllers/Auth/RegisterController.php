<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Role;
use App\Events\WelcomeUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath()
    {
        $logged_in_user = Auth::user();

        if ($logged_in_user->isTeacher())
            return route('backend.teacher.dashboard');

        if ($logged_in_user->isStudent())
            return route('frontend.student.dashboard');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisterationForm()
    {
        return view('auth.register')
            ->withRoles(Role::orderBy('name','desc')->get());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles'     =>  ['required', 'exists:roles,id'],
        ]);

        if ($data['roles'] == Role::findByType(config('access.users.teacher'))->id) {
            $rules->department = ['required'];
        }

        return $rules;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $user = User::create([
                'name'       => $data['name'],
                'email'      => $data['email'],
                'password'   => Hash::make($data['password']),
            ]);

            if ($user) {

                $user->syncRoles($data['roles']);
                $this->syncPermissions($user);

                new WelcomeUser($user);
                return $user;
            }
        });
    }

    /**
     * Sync user's permissions according to user's role
     *
     * @param \App\User $user
     */
    protected function syncPermissions($user) {
        if($user->isTeacher()){
            $user->givePermissionTo(['view backend']);

        } elseif ($user->isStudent()) {
            $user->givePermissionTo('view frontend');
        }

        return true;
    }
}
