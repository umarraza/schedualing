<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Backend\Event\StoreEventRequest;
use App\Http\Requests\Backend\Event\UpdateEventRequest;

class EventController extends Controller
{
    /**
     * var $event
     */
    protected $event;

    /**
     * @param App\Models\Event $event
     */
    public function __construct(Event $event) {
        $this->event = $event;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_in_user = Auth::user();

        if ($logged_in_user->isTeacher())
            return view('backend.events.index')->withEvents(Event::all());
        elseif($logged_in_user->isStudent())
            return view('frontend.events.index')->withEvents(Event::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventRequest $request)
    {
        if ($this->event->store($request)) 
            return redirect()->route('backend.teacher.event.index')->withFlashSuccess(__('Event was created successfully!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $logged_in_user = Auth::user();

        if ($logged_in_user->isTeacher())
            return view('backend.events.show')->withEvent($event);
        elseif ($logged_in_user->isStudent()) 
            return view('frontend.events.show')->withEvent($event);
    }

    /**
     * Edit event screen
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event) {
        return view('backend.events.edit')->withEvent($event);
    }

     /**
     * Update the given resource
     * 
     * @param Illuminate\Http\UpdateEventRequest $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventRequest $request, Event $event) {

        if ($event->update($request->all()))
            return redirect()->route('backend.teacher.event.index')->withFlashSuccess(__('Event was updated successfully!'));
    }
}
