<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Schedual;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var $schedual
     */
    protected $schedual;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Schedual $schedual)
    {
        $this->schedual = $schedual;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the teacher dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function teacherDashboard()
    {
        $scheduals = $this->schedual->where('teacher_id', auth()->user()->id)
            ->whereDate('created_at', Carbon::today())
            ->get();

        if (count($scheduals) > 0) {

            $slots = $this->schedual->getAllSlots();
            $slots = $this->schedual->convertSlotsTo12HFomat($slots);

            return view('backend.dashboard')
                ->withDate(Carbon::today())
                ->withSlots($slots);
        }

        return view('backend.dashboard');
    }

    /**
     * Show the teacher dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function studentDashboard()
    {
        return view('frontend.dashboard');
    }
}
