<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Schedual;
use App\Models\SlotBooking;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\User\ImportSchedualRequest;

class SchedualController extends Controller
{
    /**
     * @var $schedual
     */
    protected $schedual;

    /**
     * @var $schedual
     */
    protected $booking;

    /**
     * SchedualController cunstructor
     * 
     * @param App\Models\Schedual $schedual
     */
    public function __construct(Schedual $schedual, SlotBooking $booking) {
        $this->schedual = $schedual;
        $this->booking = $booking;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function teacherSchedualHistroy() {

        $bookings = $this->booking->getApprovedBookigs();

        return view('backend.scheduals.history')
            ->withBookings($bookings);
    }

    /**
     * edit a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        return view('backend.scheduals.edit');
    }

    /**
     * edit a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        return view('backend.scheduals.edit');
    }

    /**
     * Display teacher's schedual's slots
     *
     * @return \Illuminate\Http\Response
     */
    public function teacherSchedualSlots(User $teacher) {

        $scheduals = $this->schedual->where('teacher_id', $teacher->id)
            ->whereDate('date', Carbon::today())
            ->get();
        
        $slotsArray = [];

        foreach($scheduals as $schedual) {
            $slotsArray[] = $this->schedual->getBookedSlots($schedual);
        }

        $booked_slots   = \Arr::collapse($slotsArray);
        $all_slots      = $this->schedual->getAllSlots();
        $all_slots      = $this->schedual->convertSlotsTo12HFomat($all_slots);
        $slots          = $this->schedual->getFreeSlots($all_slots, $booked_slots);

        if (count($scheduals) > 0) {
            return view('teachers.slots')
                ->withSlots($slots)
                ->withTeacher($teacher)
                ->withDate(Carbon::today())
                ->withSchedual($this->schedual);
        }

        return view('teachers.slots')
            ->withTeacher($teacher)
            ->withDate(Carbon::today())
            ->withSchedual($this->schedual);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(ImportSchedualRequest $request) {

        $schedual = Schedual::importTeacherSchedual($request);

        if (!is_bool($schedual) && $schedual['errors']) {
            return redirect()->back()->withErrors($schedual['errors']);
        }            
            
        if ($schedual)
            return redirect()->back()->withFlashSuccess(__('Schedual has been imported successfully!'));
        else 
            return redirect()->back()->withFlashDanger(__('File type must be of xlsx type!'));
    }
}