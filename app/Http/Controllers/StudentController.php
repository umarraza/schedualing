<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\Role;
use App\Models\Schedual;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\User\UpdateUserRequest;
use App\Http\Requests\Frontend\User\SearchtTeachersRequest;


class StudentController extends Controller
{
    /**
     * @var App\Models\User
     */
    protected $student;

    /**
     * @var App\Models\Schedual
     */
    protected $schedul;

    /**
     * StudentController constructor.
     *
     * @param App\Models\User $student
     */
    public function __construct(User $student, Schedual $schedul)
    {
        $this->student = $student;
        $this->schedul = $schedul;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $student)
    {
        return view('frontend.profile.edit')
            ->withStudent($student);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function studentSchedualHistroy() {

        $bookings = $this->student->getApprovedBookings();

        return view('frontend.scheduals.history')
            ->withBookings($bookings);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function studentRequestSchedualHistroy() {

        $bookings = $this->student->getPendingAndRejectedBookings();

        return view('frontend.scheduals.request-history')
            ->withBookings($bookings);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Frontend\User\UpdateUserRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if ($user->isUpdated($request))
            return redirect()->route('frontend.student.dashboard')->withFlashSuccess(__('Profile was updated successfully'));
        else
            return redirect()->route('frontend.student.dashboard')->withFlashDanger(__('Profile was not updated, try again'));

    }

    /**
     * Search teacher from the system to book a slot for class.
     *
     * @param  \App\Http\Requests\Frontend\User\SearchtTeachersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function searchTeachers(SearchtTeachersRequest $request) {
        
        if ($request->ajax()) {

            $role = Role::findByType(config('access.users.teacher'));

            $teachers = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                ->where('name', 'LIKE', '%' . $request->get('name') . '%')
                ->where('role_id','=',$role->id)
                ->get();

            return view('frontend.includes.teachers')
                ->withTeachers($teachers);
        }
    }

    /**
     * Student books a slot against schedual given by teacher.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookSlot(Request $request) {
        
        if ($request->ajax()) {

            if ($this->schedul->ifSlotBookedBefore($request)) {
                return response()->json(['warning' => 'Request has already been sent for this slot!']);
            }
            
            $booking = $this->schedul->bookStudentSlot($request);

            if ($booking) {
                return response()->json(['success' => 'Request has been sent to teacher for your booking!']);
            }
        }
    }
}
