<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Carbon\Carbon;
use App\Models\Schedual;
use App\Models\SlotBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Backend\User\UpdateUserRequest;
use App\Http\Requests\Backend\User\RejectBookingRequest;
use App\Http\Requests\Backend\User\ConfirmBookingRequest;
use App\Http\Requests\Backend\User\UploadedSchedualRequest;


class TeacherController extends Controller
{
     /**
     * @var App\Models\User
     */
    protected $user;

    /**
     * @var App\Models\Schedual
     */
    protected $schedual;

    /**
     * TeacherController constructor.
     *
     * @param App\Models\User $user
     * @param App\Models\Schedual $schedual
     */
    public function __construct(User $user, Schedual $schedual)
    {
        $this->user = $user;
        $this->schedual = $schedual;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $teacher)
    {
        return view('backend.profile.edit')
            ->withTeacher($teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Backend\User\UpdateUserRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if ($user->isUpdated($request))
            return redirect()->route('backend.teacher.dashboard')->withFlashSuccess(__('Profile was updated successfully'));
        else 
            return redirect()->route('backend.teacher.dashboard')->withFlashDanger(__('Profile was not updated, try again'));
    }

    /**
     * Get all booking requests against a teacher
     * 
     * @param #request
     * 
     * @return \Illuminate\Http\Response
     */
    public function bookingRequests(Request $request) {

        $bookings = SlotBooking::where('teacher_id', Auth::user()->id)
            ->whereDate('created_at', Carbon::today())
            ->where('status', SlotBooking::PENDING)
            ->get();

        return view('backend.bookings.index')
            ->withBookings($bookings);
    }
    
    /**
     * Teacher accepts booking request from student
     * 
     * @param #request
     * 
     * @return \Illuminate\Http\Response
     */
    public function confirmBooking(ConfirmBookingRequest $request) {

        if ($request->ajax()) {
            
            if ($this->schedual->bookingConfirmed($request)) {

                $bookings = SlotBooking::where('teacher_id', Auth::user()->id)
                    ->whereDate('created_at', Carbon::today())
                    ->where('status', SlotBooking::PENDING)
                    ->get();

                return view('backend.bookings.includes.bookings-tabel')
                    ->withBookings($bookings);
            }          
        }
    }

    /**
     * Teacher reject booking request from student
     * 
     * @param #request
     * 
     * @return \Illuminate\Http\Response
     */
    public function rejectBooking(Request $request, SlotBooking $booking) {

        $booking->status = SlotBooking::REJECTED;

        if($booking->save()) {

            return redirect()->route('backend.teacher.booking.requests')->withFlashSuccess(__('Booking Request was rejected'));
        }
    }

    /**
     * Uploaded scheduals history
     * 
     * @param #request
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {

        return view('backend.scheduals.show');
    }

    /**
     * Teacher reject booking request from student
     * 
     * @param #request
     * 
     * @return \Illuminate\Http\Response
     */
    public function uploadedSchedual(UploadedSchedualRequest $request) {

        if ($request->ajax()) {

            $date = new DateTime($request->date);

            $schedual = $this->schedual->where('teacher_id', Auth::user()->id)
                ->whereDate('date', $date)
                ->where('student_id', null)
                ->first();
            
            if (empty($schedual)) {
                return [
                    'error' => 'No Schedule Found!',
                    'code' => 422,
                ];
            }

            $slots = $this->schedual->getBookedSlots($schedual);
            return view('backend.scheduals.includes.uploaded-schedual')
                ->withSlots($slots[0])
                ->withSchedual($schedual);
        }
    }
}
