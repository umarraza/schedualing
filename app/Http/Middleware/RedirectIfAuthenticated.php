<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            
            $logged_in_user = Auth::user();

            if ($logged_in_user->isTeacher())
                return route('backend.teacher.dashboard');
    
            if ($logged_in_user->isStudent())
                return route('frontend.student.dashboard');
        }

        return $next($request);
    }
}
