<?php

namespace App\Http\Requests\Backend\Event;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest.
 */
class UpdateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->isTeacher()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'name'          =>  ['required', 'max:191'],
            'location'      =>  ['required', 'max:191'],
            'start_date'    => 'required|date|after:yesterday',
            'end_date'      => 'required|date|after:yesterday',
            'description'   =>  ['required', 'max:500'],
        ];
    }
}