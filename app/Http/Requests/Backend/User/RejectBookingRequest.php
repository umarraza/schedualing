<?php

namespace App\Http\Requests\Backend\User;

use App\Models\Role;
use App\Models\SlotBooking;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class RejectBookingRequest.
 */
class RejectBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->isTeacher()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'id' => 'required|exists:slot_bookings,id',
            'status' => [ 'required', Rule::in([SlotBooking::ACCEPTED, SlotBooking::REJECTED])],
        ];
    }
}
