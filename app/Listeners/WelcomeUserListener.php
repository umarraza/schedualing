<?php

namespace App\Listeners;

use App\Mail\WelcomeUser;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserEventListener.
 */
class WelcomeUserListener
{
    /**
     * @param $event
     */
    public function handle($event)
    {
        Mail::to($event->user->email)->send(new WelcomeUser($event->user));
    }
}
