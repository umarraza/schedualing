<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait RoleMethod.
 */
trait RoleMethod
{
    /**
     * @return string
     */
    public function teacher() {
        return config('access.users.teacher');
    }

    /**
     * @return string
     */
    public function student() {
        return config('access.users.student');
    }

    public static function findByType($type){
        return self::where('name',$type)->first();
    } 
}
