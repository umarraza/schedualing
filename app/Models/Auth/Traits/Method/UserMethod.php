<?php

namespace App\Models\Auth\Traits\Method;

use Image;
use App\User;
use App\Models\Schedual;
use App\Models\SlotBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Trait UserMethod.
 */
trait UserMethod
{
    /**
     * @return mixed
     */
    public function isTeacher()
    {
        return $this->hasRole(config('access.users.teacher'));
    }

    /**
     * @return mixed
     */
    public function isStudent()
    {
        return $this->hasRole(config('access.users.student'));
    }

    /**
     * @return bool
     */
    public function isBackendUser()
    {
        return $this->hasAnyPermission(['view backend']);
    }

    /**
     * @return bool
     */
    public function isFrontendUser()
    {
        return $this->hasAnyPermission(['view frontend']);
    }

    /**
     * @return mixed
     */
    public function schedual()
    {
        return $this->hasMany(Schedual::class);
    }

    /**
     * @return mixed
     */
    public function bookings()
    {
        return $this->hasMany(SlotBooking::class,'student_id','id');
    }

    /**
     * Get approved bookings of a student
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function getApprovedBookings() {

        return SlotBooking::where('student_id', auth()->user()->id)
            ->where('status', SlotBooking::ACCEPTED)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * Get pending/rejected bookings of a teacher
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function getPendingAndRejectedBookings() {

        return SlotBooking::where('student_id', auth()->user()->id)
            ->where('status', SlotBooking::REJECTED)
            ->orWhere('status', SlotBooking::PENDING)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function avatar() {

        if (!empty($this->avatar_location))
            return asset('public/avatars/'.auth()->user()->avatar_location);
        else
            return asset('public/avatars/default.png');
    }

    /**
     * Update User/Teacher
     * 
     * @param Illuminate\Http\Request $request
     */
    public function isUpdated(Request $request) {
        
        if ($request->hasFile('avatar')) {
            
            $avatar = $request->file('avatar');
            $filename = time() . '.'. $avatar->getClientOriginalExtension();
            
            $avatar->move(public_path('/avatars/'), $filename);

            $user = Auth::user();

            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->avatar_location  = $filename;

            return $user->save() ? true : false;
        }

        return Auth::user()->update($request->all()) ? true : false;
    }
}