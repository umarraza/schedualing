<?php

namespace App\Models;

use DateTime;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'location',
        'start_date',
        'end_date',
        'description',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['start_date','end_date'];

    /**
     * Create an event
     * 
     * @param Illuminate\Http\Request $request
     * @return bool
     */
    public function store(Request $request) : bool {

        $start_date = new DateTime($request->start_date);
        $end_date   = new DateTime($request->end_date);

        $start_date = $start_date->format('Y-m-d H:i:s');
        $end_date   = $end_date->format('Y-m-d H:i:s');

        $event = Event::create([
            'name'        => $request->name,
            'user_id'     => auth()->user()->id,
            'location'    => $request->location,
            'start_date'  => $start_date,
            'end_date'    => $end_date,
            'description' => $request->description,
        ]);

        return $event ? true : false;
    }

    /**
     * @return mixed
     */
    public function getStartDateLabelAttribute() {
        return '<span class="badge bg-primary">'.$this->start_date->toFormattedDateString().'</span>';
    }

    /**
     * @return mixed
     */
    public function getEndDateLabelAttribute() {
        return '<span class="badge bg-danger">'.$this->end_date->toFormattedDateString().'</span>';
    }

    /**
     * @return mixed
     */
    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = new DateTime($value);
    }

    /**
     * @return mixed
     */
    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = new DateTime($value);
    }
}
