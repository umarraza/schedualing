<?php

namespace App\Models;

use OwenIt\Auditing\Auditable;
use App\Models\Auth\Traits\Method\RoleMethod;
use Spatie\Permission\Models\Role as SpatieRole;
use App\Models\Auth\Traits\Attribute\RoleAttribute;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Role.
 */
class Role extends SpatieRole 
{
    use RoleMethod;
}
