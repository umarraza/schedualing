<?php

namespace App\Models;

use App\User;
use DateTime;
use Validator;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Slot;
use App\Custom\SimpleCSV;
use App\Custom\SimpleXLSX;
use App\Models\SlotBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Schedual extends Model
{
    protected $table = 'scheduals';

    const TYPE_XLSX = 'xlsx';
    const TYPE_CSV = 'csv';
    const BOOKED = 1;
    const PENDING = 0;


    protected $fillable = [
        'id',
        'teacher_id',
        'student_id',
        'date',
        'start_time',
        'end_time',
        'class_name',
        'created_at',
        'updated_at',
    ];


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Import teacher schedual
     * 
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public static function importTeacherSchedual(Request $request) {

        $file_extension  = $request->schedual_file->getClientOriginalExtension();
        $file_name       = $request->schedual_file->getClientOriginalName();
        
        $request->schedual_file->move(storage_path('app/public/imports/'), $file_name);

        $filePath = storage_path('app/public/imports/'.$file_name);

        if ($file_extension == self::TYPE_XLSX) {
            if ( $xlsx = SimpleXLSX::parse($filePath) ) {

                $rows = $xlsx->rows();
                $collection = collect($rows[0]);
                $combined = $collection->combine($rows[1]);

                return self::importData($combined->all());
            }
        } 
        return false;
    }

    /**
     * Create database entries for uploaded schedual
     * @param $data
     * 
     * @return bool
     */
    protected static function importData($data) {

        $validator = Validator::make($data, [
            'date'       => 'required|date|after:yesterday',
            'start_time' => 'required|date:H:i:s',
            'end_time'   => 'required|date:H:i:s',
            'class_name' => 'required|max:50|min:3',
        ]);
        
        if($validator->fails()){
            return [
                'errors' => $validator->errors(),
            ];
        }

        $schedual = self::create([
            'teacher_id' => auth()->user()->id,
            'date'       => $data['date'],
            'start_time' => $data['start_time'],
            'end_time'   => $data['end_time'],
            'class_name' => $data['class_name'],
        ]);

        return $schedual ? true : false;
    }


    /**
     * Get booked slots in given schedual of teacher
     * @param $schedual
     * 
     * @return $slots
     */
    public function getBookedSlots($schedual) {

        $start_time     = strtotime($schedual->start_time);
        $start_date     = getDate($start_time);

        $end_time       = strtotime($schedual->end_time);
        $end_date       = getDate($end_time);

        $startTime = new DateTime;
        $startTime->setTime($start_date['hours'], $start_date['minutes'], $start_date['seconds']);

        $endTime = clone $startTime;
        $endTime->setTime($end_date['hours'], $end_date['minutes'], $end_date['seconds']);

        // Set slots' default interval of 30 minutes
        $interval   = new DateInterval('PT30M');
       
        $range = new DatePeriod($startTime, $interval, $endTime);

        foreach($range as $slot) {

            $slots[] = $slot->format('H:i:s');
        }

        $slotsArray[] = $slots;

        return $slotsArray;
    }


    /**
     * Get all slots from 9 to 6 allowed time period
     * 
     * @return $slots
     */
    public function getAllSlots() {
        $startTime = new DateTime;
        $startTime->setTime(9, 0, 0);

        $endTime = clone $startTime;
        $endTime->setTime(18, 0, 0);

        // Set slots' default interval of 30 minutes // later change the gap by giving from user
        $interval = new DateInterval('PT30M');
        $slots = new DatePeriod($startTime, $interval, $endTime);

        foreach ($slots as $slot) {
            $all_slots[] = $slot->format('H:i:s');
        }

        return $all_slots;
    }


    /**
     *  Convert given slots in 12H format 
     *  
     *  @return slots
     */ 
    public function convertSlotsTo12HFomat($slots) {

        foreach($slots as $slot) {
            $formated_slots[]  = date("h:i:s", strtotime($slot));
        }
        return $formated_slots;
    }

    /**
     *  Get all free slots in given total slots of schedual 
     *  
     *  @param $all_slots
     *  @param $booked_slots
     * 
     *  @return free_slots
     */ 
    public function getFreeSlots($all_slots, $booked_slots) {

        foreach($booked_slots as $slots) {
            foreach($slots as $booked_slot)  {
                if(in_array($booked_slot, $all_slots))
                {   
                    $key = array_search($booked_slot, $all_slots); // $key = 2;
                    unset($all_slots[$key]);
                }
            }
            $free_slots = array_values($all_slots);
        }
        return $free_slots;
    }

    /**
     *  Check if slot is available 
     *  
     *  @param $all_slots
     *  @param $booked_slots
     * 
     *  @return all_slots
     */ 
    public function isSlotAvailable($item, $id) {

        $scheduals = self::where('teacher_id', $id)
            ->whereDate('created_at', Carbon::today())->get();

        foreach ($scheduals as $schedual) {
            $date_range[] = $this->getBookedSlots($schedual);
        }
        foreach($date_range as $range) {
            foreach($range as $slot) {
                $booked_slots[] = $slot->format('H:i:s');
            }
        }

        return \in_array($item, $booked_slots) ? false : true;
    }

    /**
     * Convert a time intervel into a defined gap slots
     * 
     * @param $duration
     * @param $start_time
     * @param $end_time
     * 
     * @return $slots
     */
    // public function getServiceScheduleSlots($duration, $start_time,$end_time) {
    public function getServiceScheduleSlots($freeSlots) {
    
        $i=0;
        while($i < count($freeSlots)){

            if(isset($freeSlots[$i+1])) {
                $time[$i]['start_time'] = $freeSlots[$i];
                $time[$i]['end_time'] = $freeSlots[$i+1];
            }
            $i++;
        }

        return $time;
    }


    /**
     * Student books a slot
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function bookStudentSlot(Request $request) : bool {

        $start_time = new DateTime($request->start_time);
        
        $end_time = clone $start_time;

        $end_time   = $end_time->add( new DateInterval('PT30M'));
        $start_time = $start_time->format('H:i:s');
        $end_time   = $end_time->format('H:i:s');

        $booking = SlotBooking::create([
            'student_id' => auth()->user()->id,
            'teacher_id' => $request->id,
            'start_time' => $start_time,
            'end_time'   => $end_time,
        ]);

        return $booking ? true : false;
    }
    
    /**
     * Admin accepts booking request
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function bookingConfirmed(Request $request) : bool {

        $model = SlotBooking::find($request->id);

        $model->status = SlotBooking::ACCEPTED;
        $model->save();

        $start_time = new DateTime($model->start_time);
        
        $end_time = clone $start_time;

        $end_time   = $end_time->add( new DateInterval('PT30M'));
        $start_time = $start_time->format('H:i:s');
        $end_time   = $end_time->format('H:i:s');

        $booking = self::create([
            'date'       => date('Y-m-d'),
            'teacher_id' => $model->teacher_id,
            'student_id' => $model->student_id,
            'start_time' => $start_time,
            'end_time'   => $end_time,
        ]);
        return $booking ? true : false;
    }

    /**
     * Chek if slot already booked or not
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function ifSlotBookedBefore(Request $request) {

        $booking = SlotBooking::where('teacher_id', $request->id)
            ->whereDate('created_at', Carbon::today())
            ->where('student_id', auth()->user()->id)
            ->where('start_time', $request->start_time)
            ->first();
    
        return $booking ? true : false;
    }
}
