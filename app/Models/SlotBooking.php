<?php

namespace App\Models;

use App\User;

use Illuminate\Database\Eloquent\Model;

class SlotBooking extends Model
{
    protected $table = 'slot_bookings';

    const PENDING = 0;
    const ACCEPTED = 1;
    const REJECTED = 2;

    protected $fillable = [
        'id',
        'slot_id',
        'student_id',
        'teacher_id',
        'start_time',
        'end_time',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     * @return mixed
     */
    public function student()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function getStatusLabelAttribute() {

        if ($this->status == self::ACCEPTED)
            return '<span class="badge bg-success">Accepted</span>';
        elseif ($this->status == self::REJECTED)
            return '<span class="badge bg-danger">Rejected</span>';
        else
            return '<span class="badge bg-warning">Pending</span>';
    }

    /**
     * Get approved bookings of a teacher
     * 
     * @param Illuminate\Http\Request $request
     * 
     * @return bool
     */
    public function getApprovedBookigs() {

        return self::where('teacher_id', auth()->user()->id)
            ->where('status', self::ACCEPTED)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
