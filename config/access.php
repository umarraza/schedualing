<?php

return [

    'app_name' => env('APP_NAME'),
    'default_size' => 5,
    // Configurations for the user
    'users' => [

        // The name of the super administrator role
        'admin_role' => 'administrator',
        // The name of the super administrator role
        'teacher' => 'teacher',
        // The name of the student role
        'student' => 'student',
    ],
];
