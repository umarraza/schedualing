<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlotsBookingTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'CREATE TABLE `slot_bookings` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `student_id` int(10) unsigned NOT NULL,
            `teacher_id` int(10) unsigned NOT NULL,
            `start_time` time NOT NULL,
            `end_time` time NOT NULL,
            `status` tinyint(2) NOT NULL DEFAULT 0,
            `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
            `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
            PRIMARY KEY (`id`),
            KEY `slot_id` (`start_time`),
            KEY `student_id` (`student_id`),
            KEY `teacher_id` (`teacher_id`),
            CONSTRAINT `slot_bookings_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
            CONSTRAINT `slot_bookings_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
          ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1';

        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slots_booking_tabel');
    }
}
