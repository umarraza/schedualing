<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $teacher = Role::create(['name' => config('access.users.teacher')]);
        $student = Role::create(['name' => config('access.users.student')]);
        
        $permissions = [
            'view backend',
            'view frontend',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

         //Backend Users
         $teacher->givePermissionTo($permissions);

         //Frontend Users
         $student->givePermissionTo($permissions[1]);
    }
}