@extends('layouts.guest')

@section('title', 'Login' .' | '. 'Schedualing')

@section('content')
<div class="login-box" style="position:relative; bottom:10%">
    <div class="login-logo">
        <a href="javascript::void(0)"><b>Login</b></a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form action="{{route('login')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                </div>
            </form>
            <p class="mb-1">
                <a href="{{ route('password.email.form') }}">Forgot password?</a>
            </p>
            <p class="mb-0">
                <a href="{{ route('user.register') }}" class="text-center">Register a new membership</a>
            </p>
        </div>
    </div>
</div>
@endsection
