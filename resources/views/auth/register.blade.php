@extends('layouts.guest')

@section('title', 'Register'  .' | '. 'Schedualing')

@section('content')
<div class="register-box">
    <div class="register-logo">
        <a href="javascript::void(0)"><b>Schedule</b>Managment</a>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
        <p class="login-box-msg">Register a new membership</p>
        <form action="{{ route('register') }}" method="post">
        @csrf
            <div class="input-group mb-3">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full Name"  autocomplete="name" autofocus>
            </div>
            <div class="input-group mb-3">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email"  autocomplete="email">
            </div>
            <div class="input-group mb-3">
                <select name="roles" class="custom-select" id="pick-role">
                    <option value="">Choose Role</option>
                    @foreach ($roles as $role)
                        <option value="{{ $role->id }}" <?= (old('roles') == $role->id) ? 'selected' : '' ?>>{{ ucwords(str_replace('_', ' ', $role->name)) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group mb-3">
                <select name="department" class="custom-select" id="pick-department" style="display:none">
                    <option value="">Choose Department</option>
                    <option value="department_a">Department 1</option>
                    <option value="department_b">Department 2</option>
                </select>
            </div>
            <div class="input-group mb-3">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password"  autocomplete="new-password">
            </div>
            <div class="input-group mb-3">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"  autocomplete="new-password">                
            </div>
            <div class="row">
                <div class="col-8"></div>  
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>
        <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
        </div>
  </div>
@endsection
