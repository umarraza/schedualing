<table class="table table-bordered">
    <thead>                  
        <tr>
            <th>Student Name</th>
            <th>Status</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th style="width:200px">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($bookings as $booking)
            <tr>
                <td> {{ $booking->student['name'] }} </td>
                <td> {!! $booking->status_label !!} </td>
                <td>{{ $booking->start_time }}</td>
                <td>{{ $booking->end_time }}</td>
                <td> 
                    <a href="{{ route('backend.teacher.slot.booking.confirm') }}" data-booking-id="{{ $booking->id }}" data-booking-status="1" class="btn btn-success confirm-booking">Accept</a>    
                    <a href="{{ route('backend.teacher.slot.booking.reject', $booking->id) }}" data-booking-status="2" class="btn btn-danger">Reject</a>    
                </td>
            </tr>
        @endforeach
    </tbody>
</table>