@extends('backend.layouts.app')

@section('title', 'Teacher Dashboard' .' | '. 'Scheduling')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content" id="booking-requests">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mt-5">
                        <div class="card-header">
                            <h1 class="m-0 text-dark" style="text-align:center">Booking Requests</h1>
                        </div>
                        <div class="card-body" id="bookings-requests_container">
                            @include('backend.bookings.includes.bookings-tabel', ['bookings' => $bookings])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection