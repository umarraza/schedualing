@extends('backend.layouts.app')

@section('title', 'Teacher Dashboard' .' | '. 'Scheduling')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Welcome {{auth()->user()->name}}</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4"></div>
                    <div class="col-4">
                        <div class="form-group">
                            <form action="{{ route('backend.teacher.schedual.import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <p>*(Only support xlsx format)</p>
                                <input type="file" name="schedual_file" class="">
                                <button type="submit" class="btn btn-primary mt-2" style="float:right">Upload</button>
                            </form>
                        </div>
                    </div>
                <div class="col-4"></div>
            </div>
        </div>
    </section>
    @if (!empty($slots))
        <section class="content mt-5">
            <div class="container-fluid">
                <h1 class="m-0 text-dark" style="text-align:center">Schedule Slots | {{ $date->toFormattedDateString() }} </h1>
                <div class="row mt-5">
                    @foreach ($slots as $slot)
                        <div class="col-md-2 ">
                            <div class="color-palette-set slots-color-palette-set">
                                <div class="bg-primary disabled color-palette slot-color-palette">{{ $slot }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection