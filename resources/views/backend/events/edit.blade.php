@extends('backend.layouts.app')

@section('title', 'Edit Event' .' | '. 'Scheduling')
@section('content')
<section class="content mt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <h1 class="m-0 text-dark" style="text-align:center">Edit Event</h1>
                    </div>
                    <div class="card-body">
                        <form role="form" action="{{route('backend.teacher.event.update', $event)}}" method="POST">
                            @method('patch')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Event Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$event->name}}" placeholder="Event Name">
                                </div>
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" name="location" value="{{$event->location}}" placeholder="Location">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Start Date</label>
                                    <div class="custom-file">
                                        <input type="text" class="date form-control datepicker" name="start_date" value="{{$event->start_date->format('Y-m-d')}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">End Date</label>
                                    <div class="custom-file">
                                        <input type="text" class="date form-control datepicker" name="end_date" value="{{$event->end_date->format('Y-m-d')}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Description</label>
                                    <div class="custom-file">
                                        <input class="form-control" name="description" value="{{$event->description}}">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float:right">Update</button>
                                <a href="{{ route('backend.teacher.event.index') }}" class="btn btn-danger" style="float:right; margin-right:5px">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection