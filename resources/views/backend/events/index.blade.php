@extends('backend.layouts.app')

@section('title', 'Events' .' | '. 'Scheduling')
@section('content')
<section class="content mt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-header">
                        <h1 class="m-0 text-dark" style="text-align:center">Events</h1> 
                        <a href="{{route('backend.teacher.event.create')}}" class="btn btn-primary" style="float:right">Add</a>
                    </div>
                    <div class="card-body">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{__('Event Name')}}</th>
                                    <th>{{__('Location')}}</th>
                                    <th>{{__('Start Date')}}</th>
                                    <th>{{__('End Date')}}</th>
                                    <th style="width:200px">{{__('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($events as $event)
                                    <tr>
                                        <td>{{ $event->name }}</td>
                                        <td>{{ $event->location }}</td>
                                        <td>{!! $event->start_date_label !!}</td>
                                        <td>{!! $event->end_date_label !!}</td>
                                        <td>
                                            <a href="{{ route('backend.teacher.event.show', $event->id) }}" class="btn btn-success">View</a>    
                                            <a href="{{ route('backend.teacher.event.edit', $event->id) }}" class="btn btn-primary">Edit</a>    
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection