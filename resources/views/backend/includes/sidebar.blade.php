<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="{{asset('public/assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Sheduling</span>
    </a>
  <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{auth()->user()->avatar()}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="javascript::void(0)" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.dashboard') }}" class="nav-link {{ active_class(request()->is('teacher/dashboard')) }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('backend.teacher.edit', auth()->user()->id)}}" class="nav-link {{ active_class(request()->is('teacher/*/edit')) }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Update Profile
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.slot.edit') }}" class="nav-link {{ active_class(request()->is('teacher/schedual/edit')) }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>
                            Slots Length in Minutes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.schedual.histroy') }}" class="nav-link {{ active_class(request()->is('teacher/schedual/history')) }}">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Schedule History
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.booking.requests') }}" class="nav-link {{ active_class(request()->is('teacher/bookings/requests')) }}">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Booking Requests
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.schedual.show') }}" class="nav-link {{ active_class(request()->is('teacher/schedual/show')) }}">
                        <i   class="nav-icon fas fa-edit"></i>
                        <p>
                            Uploaded Schedule
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('backend.teacher.event.index') }}" class="nav-link {{ active_class(request()->is('teacher/event')) }}">
                        <i   class="nav-icon fas fa-edit"></i>
                        <p>
                            Events
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>  