<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="@yield('meta_author', 'Umar Raza | umarraza2200@gmail.com')">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', config('access.app_name'))</title>

    <!-- Styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/summernote/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/dist/css/toastr.css')}}">
    <link rel="stylesheet" href="{{ asset('public/css/custom.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">

    <div class="wrapper">
        @include('backend.includes.header')
        @include('backend.includes.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>

    <!-- Global Scripts -->
    <script src="{{asset('public/assets/plugins/jquery/jquery.min.js')}}"></script>
    @include('includes.messages')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('public/js/plugins.js')}}"></script>
    
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    {{-- <script src="{{asset('public/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script> --}}
    <script src="{{asset('public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/sparklines/sparkline.js')}}"></script>
    <script src="{{asset('public/assets/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('public/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/adminlte.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/pages/dashboard.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/demo.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/toastr.js')}}"></script>
    <script src="{{asset('public/js/teacher.js')}}"></script>
</body>
</html>



