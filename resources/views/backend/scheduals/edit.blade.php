@extends('backend.layouts.app')

@section('title', 'Update Slot' .' | '. 'Scheduling')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12" style="text-align:center">
                <h1 class="m-0 text-dark">Update Schedule Slots Gap</h1>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <form action="{{ route('backend.teacher.slot.update') }}" method="post">
                    @csrf
                    <div class="form-group" style="text-align:center">
                        <label>Select Slots Length</label>
                        <select name="slot-length" class="form-control">
                            <option value="00:30:00">30 Minutes</option>
                            <option value="01:00:00">60 Minutes</option>
                            <option value="01:30:00">90 Minutes</option>
                            <option value="02:00:00">120 Minutes</option>
                        </select>
                    </div>
                    <div class="offset-sm-2 col-sm-10 mt-3">
                        <button type="submit" class="btn btn-danger" style="float:right">Update</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</section>
@endsection