@if (!empty($slots))
    <section class="content mt-5">
        <div class="container-fluid">
            <h3>Class Name: {{ $schedual->class_name }}</h3>
            <div class="row mt-5">
                @foreach ($slots as $slot)
                    <div class="col-md-2 ">
                        <div class="color-palette-set slots-color-palette-set">
                            <div class="bg-primary disabled color-palette slot-color-palette">{{ $slot }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif