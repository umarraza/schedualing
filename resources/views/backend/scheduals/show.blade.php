@extends('backend.layouts.app')

@section('title', 'Uploaded Schedules' .' | '. 'Schedualing')

@section('content')
<section class="content mt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-body" style="text-align:center">
                       <h1>Uploaded Schedule Slots</h1>
                        <div class="container">
                            <form action="{{ route('backend.teacher.uploaded.scheduals') }}" method="post" id="shcedual-search">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Select Date:</label>
                                        <input type="text" class="date" name="date" id="datepicker" autocomplete="off">
                                    </div>
                                    <div class="col-sm-8 mt-3">
                                        <button type="submit" class="btn btn-danger" style="float:right">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="uploaded-schedaul_container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection