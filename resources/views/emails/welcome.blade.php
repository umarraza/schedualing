@component('mail::message')
# Welcome {{ $user->name }}

You have been registered in our system successfully.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
