@extends('frontend.layouts.app')

@section('title', 'Student Dashboard'  .' | '. 'Scheduling')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Welcome {{ auth()->user()->name }}</h1>
            </div>
        </div>
    </div>
</div>

<div id="studentDashboard">
    <section class="content">
    <div class="col-4" style="margin-left:auto;margin-right:auto;">
        <form action="{{ route('frontend.student.teachers.search') }}" method="post" id="search-teachers_form">
            @csrf
            <div class="text-center">
                <h3> Search Teacher or Department</h3>
            </div>
            <input type="text" name="name" class="form-control" class="search-teacher" id="inputName">
            <div class="offset-sm-2 col-sm-10 mt-3">
                <button type="submit" class="btn btn-danger" style="float:right">Search</button>
            </div>
        </form>
    </div>
    </section>
    <section class="content" style="margin-top:5em !important">
        <div class="row">
            <div class="col-4"></div>
                <div class="col-4">
                    <div id="teachers-container"></div>
                </div>
            <div class="col-4"></div>
        </div>
    </section>
</div>
@endsection