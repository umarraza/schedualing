@extends('frontend.layouts.app')

@section('title', 'Events' .' | '. 'Scheduling')
@section('content')
<section class="content mt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-header">
                        <h1 class="m-0 text-dark" style="text-align:center">Event</h1> 
                    </div>
                    <div class="card-body">
                        <div class="post">
                            <h3> {{$event->name}}</h3>
                            <span class="description">Start Date - {!! $event->start_date_label !!}</span>
                            <span class="description">End Date - {!! $event->end_date_label !!}</span>
                            <h3>Description</h3>
                            <p>{{$event->description}}</p>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection