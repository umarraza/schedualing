<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="index3.html" class="brand-link">
		<img src="{{asset('public/assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
			style="opacity: .8">
		<span class="brand-text font-weight-light">Sheduling</span>
	</a>
	<div class="sidebar">
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="{{auth()->user()->avatar()}}" class="img-circle elevation-2" alt="User Image">
			</div>
			<div class="info">
				<a href="javascript::void(0)" class="d-block">{{auth()->user()->name}}</a>
			</div>
		</div>
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a href="{{ route('frontend.student.dashboard') }}" class="nav-link {{ active_class(request()->is('student/dashboard')) }}">
						<i class="nav-icon fas fa-th"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{route('frontend.student.edit', auth()->user()->id)}}" class="nav-link {{ active_class(request()->is('student/*/edit')) }}">
						<i class="nav-icon fas fa-th"></i>
						<p>
							Update Profile
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('frontend.student.schedual.histroy') }}" class="nav-link {{ active_class(request()->is('student/schedual/history')) }}">
						<i class="nav-icon fas fa-copy"></i>
						<p>
							Schedule History
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('frontend.student.request.schedual.histroy') }}" class="nav-link {{ active_class(request()->is('student/schedual/request/history')) }}">
						<i class="nav-icon fas fa-edit"></i>
						<p>
							Schedule Requested History
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('frontend.student.event.index') }}" class="nav-link {{ active_class(request()->is('student/event')) }}">
						<i   class="nav-icon fas fa-edit"></i>
						<p>
							Events
						</p>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</aside>  