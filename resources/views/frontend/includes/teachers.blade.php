<table class="table table-bordered">
    <thead>                  
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($teachers as $teacher)
        <tr>
            <td><a href="{{ route('teacher.schedual.slots',$teacher->id) }}">{{ $teacher->name }}</a></td>
            <td>{{ $teacher->email }}</td>
        </tr>
        @endforeach
    </tbody>
</table>