<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="@yield('meta_author', 'Umar Raza | umarraza2200@gmail.com')">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('access.app_name'))</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/fontawesome-free/css/all.min.css')}}">
    {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('public/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/dist/css/toastr.css')}}">
</head>
<body class="hold-transition login-page">

    @yield('content')

    <script src="{{asset('public/assets/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/toastr.js')}}"></script>
    @include('includes.messages')
    <script type="text/javascript">
        $(function() {

            $('.register-box').find('#pick-role').on('change', function() {
                if ($(this).val() == 1)
                    $('.register-box').find('#pick-department').css('display', 'block')
                else
                    $('.register-box').find('#pick-department').css('display', 'none')
            });
        });
    </script>
</body>
</html>
