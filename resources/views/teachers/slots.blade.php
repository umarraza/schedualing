@extends('frontend.layouts.app')

@section('title', 'Teacher Schedual Slots' .' | '. 'Schedualing')

@section('content')
        <section class="content mt-5" id="slots-section">
            <div class="container-fluid">
                <h1 class="m-0 text-dark" style="text-align:center">Teacher Schedual Free Slots</h1>
                <p style="text-align:center"><span>(Click on slot to book)</span></p>
                @if (!empty($slots))
                    <div class="row mt-5">
                        @foreach ($slots as $slot)
                            <div class="col-md-3">
                                <div class="color-palette-set slots-color-palette-set">
                                    <div class="bg-primary disabled color-palette slot-color-palette">
                                        <a href="{{ route('frontend.student.slot.book') }}" data-teacher-id="{{ $teacher->id }}" class="book-slot">{{ $slot }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </section>
@endsection