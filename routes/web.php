<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SchedualController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// These routes does not require the user to be logged in
Route::get('/', [LoginController::class, 'showLoginForm'])->name('login');
Route::get('/user/register', [RegisterController::class, 'showRegisterationForm'])->name('user.register');
Route::post('user', [TeacherController::class, 'store'])->name('user.store');

// Password Reset Routes
Route::get('password/reset/form', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.email.form');
Route::get('user/password/reset/{token}', [ResetPasswordController::class, 'showResetForms'])->name('password.reset.form');

/*
 * These routes require the user to be logged in
 * These routes can not be hit if the password is expired
 */
Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');

/*
 * Teacher Routes
 * All route names are prefixed with 'backend.'
 * 
 */
Route::group([
    'namespace' => 'Backend', 'as' => 'backend.',
    'middleware' => [config('access.users.teacher')]
], function () {
    
    Route::get('teacher/dashboard', [HomeController::class, 'teacherDashboard'])->name('teacher.dashboard');
    Route::post('booking/accept', [TeacherController::class, 'confirmBooking'])->name('teacher.slot.booking.confirm');
    Route::get('booking/reject/{booking}', [TeacherController::class, 'rejectBooking'])->name('teacher.slot.booking.reject');
    Route::get('teacher/bookings/requests', [TeacherController::class, 'bookingRequests'])->name('teacher.booking.requests');
    
    // Event management
    Route::post('event', [EventController::class, 'store'])->name('teacher.event.store');
    Route::get('event/create', [EventController::class, 'create'])->name('teacher.event.create');
    Route::get('teacher/event', [EventController::class, 'index'])->name('teacher.event.index');
    
    Route::group(['prefix' => 'event/{event}'], function () {
        Route::get('/', [EventController::class, 'show'])->name('teacher.event.show');
        Route::get('edit', [EventController::class, 'edit'])->name('teacher.event.edit');
        Route::patch('/', [EventController::class, 'update'])->name('teacher.event.update');
    });

    // Slot management
    Route::get('edit', [SchedualController::class, 'edit'])->name('teacher.slot.edit');
    Route::post('update', [SchedualController::class, 'update'])->name('teacher.slot.update');

    // Teacher management
    Route::group(['prefix' => 'teacher/{teacher}'], function () {
        Route::get('edit', [TeacherController::class, 'edit'])->name('teacher.edit');
        Route::patch('/', [TeacherController::class, 'update'])->name('teacher.update');
    });

    // Schedule management
    Route::group([
        'prefix' => 'teacher/schedual/',
        'namespace' => 'Teacher', 'as' => 'teacher.'
    ], function () {
        Route::get('show', [TeacherController::class, 'show'])->name('schedual.show');
        Route::post('import', [SchedualController::class, 'import'])->name('schedual.import');
        Route::get('history', [SchedualController::class, 'teacherSchedualHistroy'])->name('schedual.histroy');
        Route::post('uploaded/history', [TeacherController::class, 'uploadedSchedual'])->name('uploaded.scheduals');
    });
});

/*
 * Student Routes
 * All route names are prefixed with 'frontend.'
 * 
 */
Route::group([
    'namespace' => 'Frontend', 'as' => 'frontend.',
    'middleware' => [config('access.users.student')]
], function () {

    Route::get('student/dashboard', [HomeController::class, 'studentDashboard'])->name('student.dashboard');

    Route::group(['namespace' => 'Student', 'as' => 'student.'], function () {

        Route::get('student/event', [EventController::class, 'index'])->name('event.index');
        Route::get('student/event/show/{event}', [EventController::class, 'show'])->name('event.show');

        Route::post('teachers/search', [StudentController::class, 'searchTeachers'])->name('teachers.search');
        Route::post('book/slot', [StudentController::class, 'bookSlot'])->name('slot.book');

        Route::group(['prefix' => 'student/{student}'], function () {
            Route::get('edit', [StudentController::class, 'edit'])->name('edit');
            Route::patch('/', [StudentController::class, 'update'])->name('update');
        });

        Route::group([
            'prefix' => 'student/schedual/',
        ], function () {
            Route::get('history', [StudentController::class, 'studentSchedualHistroy'])->name('schedual.histroy');
            Route::get('request/history', [StudentController::class, 'studentRequestSchedualHistroy'])->name('request.schedual.histroy');
        });
    });
});

/*
 * Routes for both teachers and students
 */
Route::group([
    'middleware' => 'auth'
], function () {
    Route::group(['namespace' => 'Teacher', 'as' => 'teacher.'], function () {
        Route::get('teacher/schedual/slots/{teacher}', [SchedualController::class, 'teacherSchedualSlots'])->name('schedual.slots');
    });
});